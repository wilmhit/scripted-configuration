#!/usr/bin/env bash

set -ex

MEM=73
CPU=26
WEIGHT=10
LXC_CUSTOM=$(cat <<EOF
lxc.mount.entry = /dev/fuse dev/fuse none bind,create=file,rw 0 0
lxc.mount.entry = /mnt/btrfs/ci-agent var/lib/docker none defaults,bind,create=dir 0 0
lxc.mount.entry = tmpfs var/lib/docker/volumes tmpfs defaults,nosuid 0 0
security.nesting = true
lxc.cap.drop =
EOF
)
