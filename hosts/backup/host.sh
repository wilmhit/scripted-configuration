#!/usr/bin/env bash

set -ex

MEM=4
CPU=4
WEIGHT=20
LXC_CUSTOM=$(cat <<EOF
lxc.mount.entry = /dev/fuse dev/fuse none bind,create=file,rw 0 0
lxc.mount.entry = /mnt/ceph-cluster/production mnt/ceph-cluster none defaults,bind,create=dir 0 0
lxc.mount.entry = / mnt/btrfs none ro,defaults,bind,create=dir 0 0
security.nesting = true

# IO optimizations as of 2024-01
# default is 100
lxc.cgroup2.io.weight = 10

EOF
)
