#!/bin/bash

#set -euo pipefail

# Script to do some garbage collection, by @fnetx
# Licenced under CC-0 (Public domain)
# get repos that have either changed in the last two days (active repositories), or have changed some time ago (so we can check if objects should be pruned)

if [ $EUID -eq 0 ]; then
	echo "Script should run as git user"
	exit 1
fi

function get_active_repos () {
	nice -n 10 find /mnt/ceph-cluster/git/gitea-repositories/ -mindepth 2 -maxdepth 2 -type d -mtime -1 | grep -v "Codeberg.quarantined"
}
function get_inactive_repos () {
	nice -n 10 find /mnt/ceph-cluster/git/gitea-repositories/ -mindepth 2 -maxdepth 2 -type d -mtime 20 | grep -v "Codeberg.quarantined"
}

echo "Garbage collecting active repos (recently changed)"
get_active_repos | while read repo; do
	cd "${repo}"
	echo "Garbage collecting active ${repo}"
	# cruft is default in newer Git versions and can likely be dropped with Git > 2.39
	nice -n 15 ionice -c 3 git -c gc.auto=100 -c pack.threads=8 -c gc.autoDetach=false gc --auto --cruft --prune=30.days.ago
	echo "Done."
	sleep 0.1
done

echo "Garbage collecting passive repos (more aggressively to optimize long-term performance)"
get_inactive_repos | while read repo; do
	cd "${repo}"
	echo "Garbage collecting inactive ${repo}"
	# cruft is default in newer Git versions and can likely be dropped with Git > 2.39
	nice -n 15 ionice -c 3 git -c gc.auto=30 -c pack.threads=8 -c gc.autoDetach=false gc --auto --cruft --prune=7.days.ago
	echo "Done."
	sleep 0.1
done
