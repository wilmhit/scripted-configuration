#!/usr/bin/env bash

set -ex

source "base/base.sh"
apt_install_norecommends docker.io apparmor caddy

source "base/users.sh"
setup_sshd
user_grant "root" "ashimokawa"
user_grant "root" "otto"
user_grant "root" "gusted"
user_grant "root" "crapstone"
user_grant "root" "javor"
user_grant "root" "xenrox"

# set up update kuma
docker pull louislam/uptime-kuma:1
docker stop uptime-kuma
docker rm uptime-kuma
docker run -d --restart=always -p 3001:3001 -v uptime-kuma:/app/data --name uptime-kuma louislam/uptime-kuma:1
docker image prune

install_file_v2 "status" "/etc/caddy/Caddyfile" || systemctl restart caddy
