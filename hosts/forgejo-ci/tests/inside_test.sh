#!/usr/bin/env bash

export PAGER=cat
RAMDISK_SIZE=1G
FORGEJO_VERSION=118.0.0-1
IP=$(hostname -I | cut -f1 -d' ')
WOODPECKER_SERVER_ADDR=$IP:8000
WOODPECKER_HOST=http://$IP:8000
WOODPECKER_ADMIN=root
WOODPECKER_REPO_OWNERS=root,testuser
FORGEJO_HOST=http://$IP:8080

function wait_for_forgejo_user() {
    rm -f /tmp/setup-forgejo.out
    local success=false
    for delay in 1 1 5 5 15 15 15 30 30 30 30 ; do
	if "$@" >> /tmp/setup-forgejo.out 2>&1 ; then
	    success=true
	    break
	fi
	cat /tmp/setup-forgejo.out
	echo waiting $delay
	sleep $delay
    done
    if test "$success" = "false" ; then
	cat /tmp/setup-forgejo.out
	return 1
    else
	grep 'Access token was successfully created' < /tmp/setup-forgejo.out | sed -e 's/.* //' > $WPBASEDIR/forgejo-root-token
	return 0
    fi
}

function setup_forgejo_user() {
    local user="$1"
    local password="$2"
    local email="$3"

    sleep 5 # for some reason trying to run "gitea admin" while forgejo is booting will permanently break everything
    if docker exec --user 1000 forgejo gitea admin user list --admin | grep "$user" ; then
	docker exec --user 1000 forgejo gitea admin user change-password --username "$user" --password "$password"
    else
	wait_for_forgejo_user docker exec --user 1000 forgejo gitea admin user create --access-token --admin --username "$user" --password "$password" --email "$email"
    fi
}

function setup_forgejo_oauth() {
    curl -XPOST -H "Content-Type: application/json" -H "Authorization: token $(cat $WPBASEDIR/forgejo-root-token)" -d '{"name":"woodpecker","redirect_uris":["'$WOODPECKER_HOST'/authorize"],"confidential_client":true}' $FORGEJO_HOST/api/v1/user/applications/oauth2 > /tmp/oauth.json
    jq -j --raw-output .client_id < /tmp/oauth.json > $WPBASEDIR/server/client_id
    jq -j --raw-output .client_secret < /tmp/oauth.json > $WPBASEDIR/server/client_secret
}

function setup_forgejo() {
    apt-get install -y jq
    mkdir -p $WPBASEDIR/server
    docker rm -f forgejo || true
    docker run --name forgejo -p 8080:3000 -e GITEA__webhook__ALLOWED_HOST_LIST=external,private,loopback -e GITEA__security__INSTALL_LOCK=true -e GITEA__log__LEVEL=debug -e GITEA__server__ROOT_URL=$FORGEJO_HOST -d codeberg.org/forgejo-integration/forgejo:$FORGEJO_VERSION
    setup_forgejo_user root admin1234 root@example.com
    sleep 5
    setup_forgejo_oauth
}

function test_setup_woodpecker() {
    #
    # Teardown
    #
    for woodpecker in woodpecker-server woodpecker-agent ; do
	systemctl stop $woodpecker || true
	systemctl disable $woodpecker || true
	userdel $woodpecker || true
	groupdel $woodpecker || true
    done
    groupdel woodpecker-shared || true
    rm -f /opt/woodpecker/server/woodpecker.sqlite
    #
    # Run
    #
    docker stop forgejo
    setup_woodpecker
    systemctl restart docker
    docker start forgejo
    sleep 5
    #
    # Test
    #
    if ! test -f /usr/bin/woodpecker-cli ; then
	CLI_ID=$(docker create --name cli-image-fetcher woodpeckerci/woodpecker-cli:${WOODPECKER_VERSION})
	docker cp "${CLI_ID}:/bin/woodpecker-cli" /usr/bin/woodpecker-cli
	docker rm cli-image-fetcher
    fi
    #
    # Creates a repository, run a job in woodpecker, check the output
    #
    # Stores a woodpecker token in /tmp/token to be used like so for debuging.
    #
    # export WOODPECKER_LOG_LEVEL=trace WOODPECKER_TOKEN="$(cat /tmp/token)" WOODPECKER_SERVER=http://$(hostname -I | cut -f1 -d' '):8000
    # woodpecker-cli info
    # curl -H "Authorization: Bearer $WOODPECKER_TOKEN" -H 'Accept: application/json' $WOODPECKER_SERVER/api/user
    #
    python3 hosts/forgejo-ci/tests/woodpecker-forgejo.py --debug --woodpecker-token-path /tmp/token --update-directory hosts/forgejo-ci/tests/test_repository $IP:8080  root admin1234 testuser test1234 testuser@example.com testproject $IP:8000

    systemctl status woodpecker-server
    systemctl status woodpecker-agent
    crontab -l | grep 'docker system prune'
}

function test_setup_goproxy() {
    #
    # Teardown
    #
    systemctl stop goproxy || true
    systemctl disable goproxy || true
    userdel goproxy || true
    #
    # Run
    #
    setup_goproxy
    #
    # Test
    #
    systemctl status goproxy
    GOPROXY=http://$GOPROXY /usr/local/go/bin/go install github.com/goproxy/goproxy/cmd/goproxy@latest
}

function test_all() {
    apt_install_forgejo
    setup_docker
    setup_forgejo
    test_setup_woodpecker
    test_setup_goproxy
}

source $(dirname $0)/../inside.sh
