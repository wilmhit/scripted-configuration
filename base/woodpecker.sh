# the path in which all woodpecker related things will be stored
: ${WPBASEDIR:=/opt/something-to-be-replaced-by-a-distinctive-name}
export WPBASEDIR
: ${WOODPECKER_SERVER_ADDR:=127.0.0.3:8000}
: ${WOODPECKER_HOST:=https://forgejo-ci.codeberg.org}
: ${WOODPECKER_ADMIN:=gapodo,dachary,fnetX,Gusted,Ryuno-Ki,fr33domlover,oliverpool,xy,realaravinth,circlebuilder,caesar}
: ${WOODPECKER_REPO_OWNERS:=forgejo,forgejo-integration,forgejo-contrib,forgejo-experimental,dachary,gapodo}
: ${FORGEJO_HOST:=https://codeberg.org}
: ${WOODPECKER_VERSION:=next-5c617accd8}

function setup_woodpecker() {
    if systemctl status woodpecker-server ; then
	return
    fi
    for client_file in client_id client_secret ; do
	client_file_path=$WPBASEDIR/server/$client_file
	if ! test -f $client_file_path ; then
	    secret_get woodpecker_$client_file
	    source base/secrets.sh
	    eval echo -n "\$woodpecker_$client_file" > $client_file_path
	fi
    done

    ######################################################################
    ## cleanup container images
    ######################################################################
    apt_install cron
    echo '@daily               docker system prune --all -f --volumes' > $WPBASEDIR/crontab
    crontab $WPBASEDIR/crontab

    ######################################################################
    ## woodpecker
    ######################################################################

    # Group IDs
    WP_G_SERVER="800"
    WP_G_AGENT="801"
    WP_G_SHARED="802"

    # User IDs
    WP_U_SERVER="800"
    WP_U_AGENT="801"

    groupadd -g $WP_G_SERVER woodpecker-server
    groupadd -g $WP_G_AGENT woodpecker-agent
    groupadd -g $WP_G_SHARED woodpecker-shared

    useradd --no-create-home --no-user-group --shell /usr/sbin/nologin --system --uid $WP_U_SERVER --gid $WP_G_SERVER --groups $WP_G_SHARED -d "$WPBASEDIR/server" woodpecker-server
    echo "woodpecker-server:*" | chpasswd -e
    useradd --no-create-home --no-user-group --shell /usr/sbin/nologin --system --uid $WP_U_AGENT --gid $WP_G_AGENT --groups $WP_G_SHARED,docker -d "$WPBASEDIR/agent" woodpecker-agent
    echo "woodpecker-agent:*" | chpasswd -e


    mkdir -p "${WPBASEDIR}"

    mkdir -p "${WPBASEDIR}/server"
    chown $WP_U_SERVER:$WP_G_SERVER "${WPBASEDIR}/server"
    chmod 750 "${WPBASEDIR}/server"

    mkdir -p "${WPBASEDIR}/agent"
    chown $WP_U_AGENT:$WP_G_AGENT "${WPBASEDIR}/agent"
    chmod 750 "${WPBASEDIR}/agent"

    mkdir -p "${WPBASEDIR}/shared"
    chown $WP_G_SERVER:$WP_G_SHARED "${WPBASEDIR}/shared"
    chmod 750 "${WPBASEDIR}/shared"

    mkdir -p "${WPBASEDIR}/bin/"
    mkdir -p "${WPBASEDIR}/helper-bin"
    mkdir -p "${WPBASEDIR}/wp-bins/"

    # Script for easy extraction of woodpecker binaries from the docker images
    cp base/woodpecker-update.sh "${WPBASEDIR}/helper-bin/update.sh"
    chmod +x "${WPBASEDIR}/helper-bin/update.sh"
    "${WPBASEDIR}/helper-bin/update.sh" ${WOODPECKER_VERSION}

    ######################################################################
    ## woodpecker server
    ######################################################################

    touch "${WPBASEDIR}/shared/agent_secret"
    chmod 640 "${WPBASEDIR}/shared/agent_secret"
    echo -n "$(openssl rand -hex 32)" > "${WPBASEDIR}/shared/agent_secret"

    touch "${WPBASEDIR}/server/client_id"
    chmod 600 "${WPBASEDIR}/server/client_id"
    touch "${WPBASEDIR}/server/client_secret"
    chmod 600 "${WPBASEDIR}/server/client_secret"

    # secrets need to be added via echo -n "" > file as everything else leaves a control character at the end
    # Alternatively create a file with the value (no new line) and do cat secretfile | awk '{printf $1}' > secretfile.without.newline
    # this is done later on in this setup, this is just a "warning"

    cat > ${WPBASEDIR}/server/env <<EOF
WOODPECKER_HOST="$WOODPECKER_HOST"
WOODPECKER_SERVER_ADDR="$WOODPECKER_SERVER_ADDR"
WOODPECKER_GRPC_ADDR="127.0.0.3:9000"
WOODPECKER_ADMIN="$WOODPECKER_ADMIN"
WOODPECKER_REPO_OWNERS="$WOODPECKER_REPO_OWNERS"
WOODPECKER_OPEN=true
WOODPECKER_ESCALATE=""
WOODPECKER_AGENT_SECRET_FILE="$WPBASEDIR/shared/agent_secret"
WOODPECKER_DATABASE_DRIVER="sqlite3"
WOODPECKER_DATABASE_DATASOURCE="$WPBASEDIR/server/woodpecker.sqlite"
WOODPECKER_LOG_LEVEL=trace
WOODPECKER_STATUS_CONTEXT="forgejo-ci"
WOODPECKER_GITEA=true
WOODPECKER_GITEA_URL="$FORGEJO_HOST"
WOODPECKER_GITEA_CLIENT_FILE="$WPBASEDIR/server/client_id"
WOODPECKER_GITEA_SECRET_FILE="$WPBASEDIR/server/client_secret"
EOF

    envsubst '$WPBASEDIR,${WPBASEDIR}' > /etc/systemd/system/woodpecker-server.service <<'EOF'
[Unit]
Description=Woodpecker-server instance
# check base required files
AssertFileNotEmpty=$WPBASEDIR/server/env
AssertFileNotEmpty=$WPBASEDIR/server/client_id
AssertFileNotEmpty=$WPBASEDIR/server/client_secret
AssertFileNotEmpty=$WPBASEDIR/shared/agent_secret

# check that our binary is executable
AssertFileIsExecutable=$WPBASEDIR/bin/woodpecker-server

[Service]
Type=simple
EnvironmentFile=$WPBASEDIR/server/env
ExecStart=$WPBASEDIR/bin/woodpecker-server
Restart=always
RestartSec=10
ExecReload=/bin/kill -HUP $MAINPID
Restart=always
User=woodpecker-server
Group=woodpecker-shared

[Install]
WantedBy=multi-user.target
EOF



    ######################################################################
    ## woodpecker agent
    ######################################################################
    envsubst '$WPBASEDIR,${WPBASEDIR}' > ${WPBASEDIR}/agent/env <<'EOF'
WOODPECKER_SERVER="127.0.0.3:9000"
WOODPECKER_AGENT_SECRET_FILE="$WPBASEDIR/shared/agent_secret"
WOODPECKER_MAX_WORKFLOWS="2"
WOODPECKER_HEALTHCHECK_ADDR="127.0.0.2:3000"
WOODPECKER_BACKEND="docker"
EOF

    envsubst '$WPBASEDIR,${WPBASEDIR}' > /etc/systemd/system/woodpecker-agent.service <<'EOF'
[Unit]
Description=Woodpecker-agent instance

# No sense in starting the agent if woodpecker and docker are not running
Requires=docker.service woodpecker-server.service
After=docker.service woodpecker-server.service

# check required files
AssertFileNotEmpty=$WPBASEDIR/agent/env
AssertFileNotEmpty=$WPBASEDIR/shared/agent_secret

# Confirm that docker is running on a mounted /var/lib/docker, else we'll kill codebergs ssds
AssertPathIsMountPoint=/var/lib/docker

# check that our binary is executable
AssertFileIsExecutable=$WPBASEDIR/bin/woodpecker-agent

[Service]
Type=simple
EnvironmentFile=$WPBASEDIR/agent/env
ExecStart=$WPBASEDIR/bin/woodpecker-agent
Restart=always
RestartSec=10
ExecReload=/bin/kill -HUP $MAINPID
Restart=always
User=woodpecker-agent
Group=woodpecker-shared

[Install]
WantedBy=multi-user.target
EOF

    ######################################################################
    ## woodpecker permission cleanup and start
    ######################################################################

    systemctl daemon-reload

    chown -R root:root "${WPBASEDIR}"
    chown -R woodpecker-server:woodpecker-server "${WPBASEDIR}/server"
    chown -R root:woodpecker-shared "${WPBASEDIR}/bin"
    chown -R woodpecker-agent:woodpecker-agent "${WPBASEDIR}/agent"
    chown -R root:woodpecker-shared "${WPBASEDIR}/shared"
    chmod 700 "${WPBASEDIR}/server"
    chmod 700 "${WPBASEDIR}/agent"
    chmod 750 "${WPBASEDIR}/shared"

    systemctl enable --now woodpecker-server
    systemctl enable --now woodpecker-agent
}
