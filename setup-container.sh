#!/usr/bin/env bash

set -ex

source "base/host.sh"
source "base/base.sh"

HOSTNAME=${1:-$(read -p "Please provide the hostname for the container to set up. " && echo "$REPLY")}
if [ -f "hosts/${HOSTNAME}.host.sh" ]; then
	source "hosts/${HOSTNAME}.host.sh"
fi
if [ -f "hosts/${HOSTNAME}/host.sh" ]; then
	source "hosts/${HOSTNAME}/host.sh"
fi
host_configure_container

case $VIRTUALIZATION in
	"lxc")
		setup_new_lxc
		;;
	"kvm")
		setup_new_kvm
		;;
esac
